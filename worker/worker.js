//When user searches for a particular employee
const {ipcRenderer}=require('electron')

$('#showemployee').on('click',()=>{
  username= $('#search').val();
  ipcRenderer.send('searchWorker',username);
})

ipcRenderer.on('searchWorker',(e,results,fields)=>{
  setEmpTable(results,fields);
})


//To show details of all the Employees
$('#showemployees').on('click',()=>{
  ipcRenderer.send('showAllWorker',true);
})


ipcRenderer.on('showAllWorker',(e,results,fields)=>{
  setEmpTable(results,fields);
})


// To set Employee Table
function setEmpTable(results,fields){
  var theads=document.getElementById('theads');
  var tbodies=document.getElementById('tbodies');

  while (theads.hasChildNodes()) {
    theads.removeChild(theads.childNodes[0]);}
  while (tbodies.hasChildNodes()) {
    tbodies.removeChild(tbodies.childNodes[0]);}
  
  var row=document.createElement('tr');
  for(var i=0;i<fields.length;i++)
  {
    var col=document.createElement('th');
    col.innerHTML=fields[i].name;
    row.appendChild(col);
  }
  
  theads.appendChild(row);

  for (var k = 0; k < results.length;k++) {
    var a=document.createElement('tr');
    myObj = results[k];
    len= Object.keys(myObj).length;
    for( var j=0;j<len;j++) {
        var b=document.createElement('td');
        b.innerHTML=myObj[fields[j].name];
        a.appendChild(b);
    }
    document.getElementById('tbodies').appendChild(a);
  }
  $('#data').DataTable();
}
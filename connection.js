var mysql      = require('mysql');

var pool = mysql.createPool({
  host     : 'localhost',
  user     : 'root',
  password : '',
  database : 'sail',
  port     : '3306'
});

module.exports = pool;
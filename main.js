const {app, BrowserWindow, ipcMain, dialog, session} = require('electron')

require('electron-reload')(__dirname)
const Swal = require('sweetalert2')

let mainWindow

let pool = require('./connection.js')
let db = require('./db.js')
let log = require('electron-log')
let defaultSession


function dbconnect(){
  pool.getConnection(function(err,connection){
    if(err){
      log.error(err);
      dialog.showMessageBox({message:'Error in Database connection\n'+err,buttons:['Quit','Retry']},(buttonIndex)=>{
        if(buttonIndex===0){
          app.quit();
        }else if(buttonIndex===1){
          dbconnect();
        }
      })
    }else{
      connection.release();
    }
  });
}

function createWindow() {
  mainWindow = new BrowserWindow({ width: 1200, height: 800, minWidth: 400, minHeight: 200, webPreferences: { nodeIntegration: true }})
  // mainWindow.loadFile('admin/signup.html') //uncomment this for first time signup and comment next line
  
  // mainWindow.loadFile('worker/worker.html')
  mainWindow.loadFile('login.html')
  // mainWindow.webContents.openDevTools()
  mainWindow.webContents.once('did-finish-load',()=>{
    defaultSession=session.defaultSession
    dbconnect();
    // defaultSession.clearStorageData(); //Comment for real use
    defaultSession.cookies.get({name:'isloggedin'},(error,cookies)=>{
      if(cookies.length!=0 && cookies[0].value=='true' ){
        defaultSession.cookies.get({name:'usertype'},(error,cookies)=>{
          if(cookies.length!=0){
            redirectUser(cookies[0].value)
          }
        })
      }
    })
  })
  mainWindow.on('closed', function () { mainWindow = null })
}

app.on('ready',createWindow)

app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') app.quit()
})

app.on('activate', function () {
  if (mainWindow === null) createWindow()
})



function redirectUser(usertype){
  if(usertype=='admin'){
    mainWindow.loadFile('admin/admin.html')
  }else if(usertype=='worker'){
    mainWindow.loadFile('worker/worker.html')
  }else if(user.usertype=='other'){
    // load file according to you
  }
}









// 
// 
// ADD CHANNELS BELOW THIS
// 
// 

ipcMain.on('loginInit',(e,loginDetail)=>{
  db.checkAuth(loginDetail,pool,(status,user)=>{
    if(status){
      let t = new Date().getTime()+(24*60*60*1000)
      defaultSession.cookies.set({url:'example.com',name:'isloggedin', value:'true',expirationDate: t})
      defaultSession.cookies.set({url:'example.com',name:'username', value:user.username,expirationDate: t})
      defaultSession.cookies.set({url:'example.com',name:'usertype', value:user.usertype,expirationDate: t})
      console.log('login successful with username: \''+user.username+'\', usertype: \''+user.usertype+'\'' );
      redirectUser(user.usertype)   
    }else{
      e.sender.send('loginInit',false);
    }
  })
})

ipcMain.on('signupUser',(e,signupDetail)=>{
  db.addUser(signupDetail,pool,(status,results)=>{
    if(status){
      // console.log('success');
      dialog.showMessageBox({buttons:['Ok'],title:'Success',message : ' Signup Successfuly'})
      e.sender.send('signupUser',true);
    }else{
      e.sender.send('signupUser',false);
      dialog.showErrorBox('Signup Unsuccessful','Some error occured (may be Username is taken)');
    }
  })
})

ipcMain.on('open-new-window',(e, fileName)=>{
  let win = new BrowserWindow({width:960, height:540, minWidth: 400, minHeight: 200, webPreferences: { nodeIntegration: true }})
  win.loadFile(fileName)
})

ipcMain.on('logout',(e,status)=>{
  if(status){
    defaultSession.clearStorageData();
    app.quit();
  }
})

ipcMain.on('getUsers',(e,getuser)=>{
  if(getuser){
    db.getUsers(pool,(status,users)=>{
      e.sender.send('getUsers',users)
    })
  }
})

// Tushar (woker::details)

ipcMain.on('searchWorker',(e,searchit)=>{
  // console.log('value accessed: '+searchit);
  db.searchWorker(searchit,pool,(status,result,fields)=>{
    if(status){
      // console.log('status: '+result)
      e.sender.send('searchWorker',result,fields);
    }else{
      e.sender.send('searchWorker',false,fields);
      dialog.showErrorBox("Search Unsuccessful","some error occured...!!!!");
    }
  })
})

ipcMain.on('showAllWorker',(e,searchit)=>{
  // console.log('value accessed: '+searchit);
  if(searchit){
    db.showAllWorker(pool,(status,result,fields)=>{
      if(status){
        console.log('status: '+result[0][0])
        e.sender.send('showAllWorker',result,fields);
      }else{
        e.sender.send('showAllWorker',false,fields);
        dialog.showErrorBox("Search Unsuccessful","some error occured...!!!!");
      }
    })
  }
})

ipcMain.on('delUser',(e,username)=>{
  db.delUser(username,pool,(status)=>{
    if(status){
      e.sender.send('delUser',true);
      dialog.showMessageBox({buttons:['Ok'],title:'Deleted',message : username+' deleted Successfuly'})
    }else{
      dialog.showErrorBox("Search Unsuccessful","some error occured...!!!!");
    }
  })
})



// 
// 
// ADD CHANNELS ABOVE THIS
// 
// 

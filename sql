CREATE DATABASE sail;
CREATE TABLE users(
	username varchar(256) PRIMARY KEY,
    pwd varchar(256),
    usertype varchar(50)
);

CREATE TABLE `employee` (
  `emp_no` int(100) NOT NULL PRIMARY KEY,
  `name` varchar(100) NOT NULL,
  `mobile` bigint(10) NOT NULL,
  `age` int(100) NOT NULL,
  `Address` text NOT NULL
);
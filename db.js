const bcrypt=require('bcrypt')
const {dialog}=require('electron')
const log=require('electron-log')

module.exports.checkAuth=function(loginDetail,pool,callback){
    var query='SELECT * FROM users WHERE username=?;';
    
    var params=[loginDetail.username];
    pool.getConnection(function(err,con){
        if(err){log.error(err);return;}
        con.query(query,params,function(error,user,fields){
            if(error){
                log.error(error);
                dialog.showErrorBox("Error","Error in Database Connection");
                return;
            }
            if(user.length>0){
                bcrypt.compare(loginDetail.pwd, user[0].pwd, function(err, res) {
                    if(err){
                        log.error(err);
                        dialog.showErrorBox("Error","Error in Encryption");
                        return;
                    }
                    if(res==true){
                        callback(true,user[0]);
                    }else{
                        callback(false,user);
                    }
                });
            }else{
                callback(false,user);
            }
        })
        con.release()
    })
}

function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max))+1;
}

module.exports.addUser=function(userDetail,pool,callback){
    var query='INSERT INTO users(username,pwd,usertype) VALUES(?,?,?);';
    
    pool.getConnection(function(err,con){
        if(err){log.error(err);return;}
        bcrypt.hash(userDetail.pwd, getRandomInt(6), function(err, hashpwd) {
            if(err){log.error(err);return;}
            var params=[userDetail.username,hashpwd,userDetail.usertype];
            con.query(query,params,function(error,results,fields){
                if(error){
                    log.error(error);
                    callback(false,results);
                }else{
                    callback(true,results);
                }
            })
        });
        con.release()
    })
}

module.exports.getUsers=function(pool,callback){
    var query='SELECT username,usertype FROM users;';
    
    pool.getConnection(function(err,con){
        if(err){log.error(err);return;}
        con.query(query,function(error,results,fields){
            if(error){
                log.error(error);
                callback(false,results);
            }else{
                callback(true,results);
            }
        })
        con.release()
    });
}

module.exports.searchWorker=function(searchit,pool,callback){
    
    var query = 'SELECT * FROM employee WHERE emp_no=? OR name LIKE "%'+searchit+'%"';
    
    pool.getConnection(function(err,con){
        if(err){log.error(err);return;}
        con.query(query,[searchit], function (error, results, fields){
            if(error){
                log.error(error);
                callback(false,results,fields);
            }else{
                callback(true,results,fields);
            }
        })
        con.release()
    })
}

module.exports.showAllWorker=function(pool,callback){
    
    var query = 'SELECT * FROM employee';

    pool.getConnection(function(err,con){
        if(err){log.error(err);return;}
        con.query(query, function (error, results, fields){
            if(error){
                log.error(error);
                callback(false,results,fields);
            }else{
                callback(true,results,fields);
            }
        })
        con.release()
    })
}

module.exports.delUser=function(username,pool,callback){
    
    var query = 'DELETE FROM users WHERE username=?;';

    pool.getConnection(function(err,con){
        if(err){log.error(err);return;}
        con.query(query,[username], function (error, results, fields){
            if(error){
                log.error(error);
                callback(false);
            }else{
                callback(true);
            }
        })
        con.release()
    })
}
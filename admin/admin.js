const {BrowserWindow, ipcRenderer, remote}=require('electron')
const Swal = require('sweetalert2')

$('#addUserBtn').on('click',()=>{
    // remote.getCurrentWindow().loadFile("admin/signup.html");
    ipcRenderer.send('open-new-window','admin/signup.html')
})

$('#manageAccBtn').on('click',()=>{
    console.log('pressed')
    ipcRenderer.send('open-new-window','admin/manageAccount.html')
})

$('#logoutBtn').on('click',()=>{
    Swal.fire({
        title: 'Are you sure?',
        text: 'You will be Logged Out!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, Logout!',
        cancelButtonText: 'No, Go on'
      }).then((result) => {
        if (result.value) {
            ipcRenderer.send('logout',true);
        }
      })
})
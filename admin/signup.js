const {ipcRenderer}=require('electron')

$(document).ready(function(){
    $('.error').hide();

    $('input').keypress(function(){
        $(this).removeClass('border-danger');
        hideNextError(this);
    })
});

$('#showpwd').on('click',()=>{
    if($('#showpwd').is(":checked")){
        $('#pwd').attr('type','text');
    }else{
        $('#pwd').attr('type','password');
    }
})


function showNextError(el){
    el.next().show();
}

function hideNextError(el){
    el=$(el).next();
    if($(el).hasClass('error'))
        $(el).hide();
}

function formValidate(){
    ret = true;

    if($('#username').val()<6 || $('#username').val()>20){
        $('#username').addClass('border-danger');
        showNextError($('#username'));
        ret= false;
    }

    if($('#pwd').val()<6 ||  $('#pwd').val()>50){
        $('#pwd').addClass('border-danger');
        showNextError($('#pwd'));
        ret=false;
    }

    if( $('#pwd').val()!= $('#cnfrmpwd').val()){
        $('#cnfrmpwd').addClass('border-danger');
        showNextError($('#cnfrmpwd'));
        ret=false;
    }

    if( $('#usertype').val()=='admin'){
        temp=confirm('Are you sure for User-type as admin?');
        if(!temp) ret=false;
    }

    return ret;
}



$('#signupBtn').on('click',()=>{
    if(formValidate()){
        $('#loadingdiv').show()
        signupDetail={
            username: $('#username').val(),
            pwd: $('#pwd').val(),
            usertype: $('#usertype').val()
        }

        ipcRenderer.send('signupUser',signupDetail);
    }
})

ipcRenderer.on('signupUser',(e,status)=>{
    $('#loadingdiv').hide();
    if(status){
        $('#signupForm')[0].reset();
    }
})